#!/usr/bin/env python3

import time

import sympy as sym

x = 0
y = 0

s = time.time()

for i in range(120):
    for j in range(120):
        area = i * j
        perim = 2 * i + 2 * j
        if area == 732 and perim == 120:
            print(i, j)

print(time.time() - s)


t = time.time()
x, y = sym.symbols('x,y')
eq1 = sym.Eq(2 * x + 2 * y, 120)
eq2 = sym.Eq(x * y, 732)
result = sym.solve([eq1, eq2], (x, y))
print(result)

print(time.time() - t)

print((75.19 // 12), (75.19 % 12))
