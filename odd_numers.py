#!/usr/bin/env python3

start = 14
end = 11948


for i in range(start, end):
    if bool(i & 1):
        print(i)
