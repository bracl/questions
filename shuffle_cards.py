#!/usr/bin/env python3

import random

cards = [x for x in range(52)]
draw = 2


def print_cards(cards):
    human = []
    for c in cards:
        suit = c // 13
        value = c % 13
        if suit == 0:
            suit = 'C'
        elif suit == 1:
            suit = 'D'
        elif suit == 2:
            suit = 'H'
        elif suit == 3:
            suit = 'S'
        else:
            assert False
        if value == 0:
            value = 'A'
        elif value == 10:
            value = 'J'
        elif value == 11:
            value = 'Q'
        elif value == 12:
            value = 'K'
        human.append(f'{value}{suit}')
    print(', '.join(human))


for _ in range(100):
    random.shuffle(cards)
    print_cards(cards[:draw])
